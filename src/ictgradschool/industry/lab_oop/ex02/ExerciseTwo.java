package ictgradschool.industry.lab_oop.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {
        int lowerBound= getInts("Enter lower bound:");
        int upperBound = getInts("Enter upper bound:");
        int r1 = getRandomInt(lowerBound, upperBound);
        int r2 = getRandomInt(lowerBound, upperBound);
        int r3 = getRandomInt(lowerBound, upperBound);
        int lowestOfr1r2 = Math.min(r1, r2);
        int lowestRandom = Math.min(lowestOfr1r2, r3);

        System.out.println("Lower bound? " + lowerBound);
        System.out.println("Upper bound? " + upperBound);
        System.out.println("3 randomly generated numbers: " + r1 + ", " + r2 + " and " + r3);
        System.out.println("Smallest number is " + lowestRandom);
    }

    private int getInts(String message) {
        System.out.println(message);
        String userInput = Keyboard.readInput();
        int newInteger = Integer.parseInt(userInput);
        return newInteger;
    }

    private int getRandomInt(int lower, int upper) {
        int randomNum = (int) (Math.random() * (upper - lower) + lower);
        return randomNum;

    }




    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
